
// // // PART 1
// // fetch('https://jsonplaceholder.typicode.com/todos')
// // .then((response) => response.json())
// // .then((json) => console.log(json));

// PART 2
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => {

	let list = json.map((todo => {
		return todo.title;
	}))

	console.log(list);

});

// PART 3
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json))

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`));

// PART 4
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	title: 'Created To Do List Item',
	  	completed: false,
	  	userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// PART 5
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	title: 'Updated To Do List Item',
	  	description: 'To update the my to do list with a different data structure.',
	  	status: 'Pending',
	  	dateCompleted: 'Pending',
	  	userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// PART 6
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	status: 'Complete',
	  	dateCompleted: '08/28/22'
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// PART 7
fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'DELETE',
});

